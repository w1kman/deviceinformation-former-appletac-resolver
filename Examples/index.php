<?php
/**
 * Example usage of class DeviceInformation
 * @author Thomas Wikman
 * NOTE: $mysqli must be a mysqli object
 */

//Establish connection to MySQL
$mysqli = new mysqli('localhost', 'username', 'password', 'database');
$mysqli->set_charset('utf8');


// Include class
require "../DeviceInformation/class.DeviceInformation.php";

// Create DeviceInformation Object
$di = new DeviceInformation($mysqli);

// Get device

# Example 1
echo $di->imei("013205000000000")->device; // Will work

# Example 2
$di->imei("013205000000000");              // Same as creating DeviceInformation($mysqli,$imei);
echo $di->device;                          // Will work

#Example 3
echo $di->device;                          // Will NOT work (unless DeviceInformation object is created with DeviceInformation($mysqli,$imei))

?>

