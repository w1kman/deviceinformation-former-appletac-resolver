<?php
/**
 * Description of DeviceInformation
 * 
 * Purpose of this class is to lookup <tac> in a MySQL database to get <device>.
 * The database only contains Apple Inc TAC's, hence it can be used to
 * verify if <imei> is an Apple Inc device
 * 
 * @author Thomas Wikman
 * @version 0.2
 * @category AutoMateSE/opcodes
 */

class DeviceInformation {
    public  $tac,
            $device,
            $imei;
    
    private $link, // MySQLi object
            $db = [
                'table' => 'table_name',
                // NOTE: Change 'columns' keys depending on 'table' column names.
                'columns' => [
                    'id',   // Represents PK
                    'tac',  // Represents TAC
                    'device',
                ],
            ];
    
    /**
     * @param object (mysqli) $link
     * @param string $imei (default: null)
     */
    public function __construct($link = null,$imei = null) {
        // Verify that $link isnt null AND is a mysqli object.
        $this->link = is_a($link, 'mysqli') ? $link : die('ERROR: No connection to MySQL.');
        // Set IMEI (function will sort out null values)
        // NOTE: function imei() for more info (shit goes down).
        $this->imei($imei);
        /**
         * @todo No need for application to DIE when $link is omitted (could still return tac and validate imei)
         * @todo Make class usable when 'imei14' is used (add function for calculating verification digit).
         */
        
    }
    /**
     * Set IMEI
     * when a valid $imei is passed to function, $this->imei, $this->tac &
     * $this->device will get populated.
     * 
     * @param int|string $imei
     * @return \DeviceInformation
     */
    public function imei($imei = null){
        if (preg_match('/^(\d){15}$/', $imei)){
            $this->imei = $imei;
        }
        $this->tac();       // set $this->tac
        $this->device();    // set $this->device
        return $this;
    }
    /**
     * Set TAC
     * when a valid $imei|$this->imei is passed to function, $this->tac will
     * get populated.
     * 
     * @param int|string $imei (if NULL $this->imei is used)
     * @return \DeviceInformation
     */
    private function tac($imei = null) {
        $imei = $imei ? $imei : $this->imei;
        // Check if $imei is digits and lenght is 15
        if (preg_match('/^(\d){15}$/', $imei)){
            // 8 first digits are the TAC, stored in array $match 
            if (preg_match('/^(\d){8}/', $imei, $match)) {
                $this->tac = (string) $match[0];
            }
        }
        return $this;
    }
    /**
     * Set DEVICE
     * when a valid $tac|$this->tac is passed to function, $this->device will
     * get populated.
     * 
     * @param string $tac (if NULL $this->tac is used)
     * @return \DeviceInformation
     */
    private function device($tac = null) {
        $tac = $tac ? $tac : $this->tac;
        if (preg_match('/^\d{8}$/', $tac, $match)) {
            $fields = "`".implode('`, `', $this->db['columns'])."`";
            $row = $this->link->query("SELECT {$fields} FROM `{$this->db['table']}` WHERE `{$this->db['columns'][1]}` = '{$match[0]}' LIMIT 1")->fetch_assoc();
        }
        $this->device = is_array($row) ? (string) $row[$this->db['columns'][2]] : false;
        return $this;
    }
}