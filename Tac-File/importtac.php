<?php
/**
 * Import new <tac> && <device> to MySQL database
 * NOTE: set `tac` to UNIQUE, unless you want duplicates in the database.
 */

/**
 * Establish connection to MySQL
 */
$mysqli = new mysqli('localhost', 'username', 'password', 'database');
$mysqli->set_charset('utf8');


// file including <tac> && <device>
// Example line: <tac>\t<imei>\n
$tac_file = "./tacs.txt"; // Using tabs as delimiter

// Open file and read line-by-line
$file = fopen($tac_file, "r");
while(!feof($file)){
    $line = fgets($file);
    $tmp = explode("\t",$line);
    if ($tmp[0]) { // Make sure not to insert null-values into the array
        // fill array with values
        $out[] = [
            'tac' => $tmp[0],
            'device' => $tmp[1],
        ];        
    }
}
fclose($file); // Close file
$i=0;$error=0;$duplicate=0;

// "Try" to insert, row-by-row
foreach ($out as $values){
    $mysqli->query("INSERT INTO `opcodes_tac` (`id`,`tac`,`device`) VALUES (null,'{$values['tac']}','{$values['device']}');");
    if (!$mysqli->error){
        $i++; // This was a success adding 1 to num of success
    }else{
        if ($mysqli->errno == "1062") { // 1062 duplicate
            $duplicate++; // This was a duplicate adding 1 to num of duplicates
            
        }else{
            $error++; // This was an error adding 1 to num of errors
            $error_array[] = $mysqli->error; // Add error (string) to array
            $error_array[] = $mysqli->errno; // Add errno (int) to array
        }
    }
}

// Get some numbers
$total = $i + $error + $duplicate;
echo "{$i} items added of {$total} (new: {$i}; duplicates: {$duplicate}; errors: {$error};) ";
?>

<pre>
<?php
// Print errors
if (is_array($error_array)){
    echo "<p>Errors</p>";
    print_r($error_array);
}
?>
</pre>
